const setupDateTimeInputs = (inputType, widgetFunction) =>
  $(`input[type="${inputType}"]`).each((i, input) => {
    const $input      = $(input)
    const $label      = $(`label[for="${$input.attr('id')}"]`)
    const $dt_input   = $('<input class="form-control" type="text">')
    const dt_input_id = `${$input.attr('id')}_dt_selector`

    $dt_input.attr('id', dt_input_id)
    $dt_input.addClass(inputType)
    $dt_input.attr('placeholder', $input.attr('placeholder'))

    widgetFunction($dt_input, $input)
    $input.change()

    $label.attr('for', dt_input_id)
    $input.hide().after($dt_input)
  })

const dateHandler = (picker, input) => {
  const $picker = $(picker)
  const $input  = $(input)

  $picker.on('change', () => {
    const date = $picker.datepicker('getDate')
    $input.val(date && date.toISOString().split('T')[0])
  })

  $picker.datepicker({
    autoclose:  true,
    format:     'M d, yyyy',
  })

  $input.on('change', () => {
    if (!$input.val()) return

    try {
      const d = $(this).val().split('-')
      $picker.datepicker('update', `${d[1]} ${d[2]}, ${d[0]}`)
    } catch (e) {
      console.log(e)
    }
  })
}

const inputIsCorrectType = inputType => {
  const elem = document.createElement('input')
  elem.setAttribute('type', inputType)
  return elem.type == inputType
}

const initForms = () => {
  const inputTypes = [{
    inputType: 'date',
    polyfill:   () => setupDateTimeInputs('date', dateHandler),
  }]

  $(inputTypes).each((i, type) => {
    if (inputIsCorrectType(type.inputType))
      type.nativeInit && type.nativeInit()
    else
      type.polyfill && type.polyfill()
  })
}

$(initForms)
