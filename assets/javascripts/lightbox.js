import PhotoSwipe from 'photoswipe'
import PhotoSwipeUI_Default from 'photoswipe/dist/photoswipe-ui-default'

const initLightbox = () => {
  const pswpElement   = document.querySelector('.pswp')
  const lightboxItems = document.querySelectorAll('.lightbox')

  const items = Array.prototype.map.call(lightboxItems, item => {
    const img = item.querySelector('img')
    return {
      src:    item.getAttribute('href'),
      msrc:   img.getAttribute('src'),
      title:  img.getAttribute('alt'),
      w:      1125,
      h:      1125,
    }
  })

  Array.prototype.forEach.call(lightboxItems, (item, index) => {
    item.addEventListener('click', e => {
      e.preventDefault()
      const gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items,
        { index })
      console.log(pswpElement, items, index)
      gallery.init()
    })
  })
}

document.addEventListener('DOMContentLoaded', initLightbox)
