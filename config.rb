# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Reload the browser automatically whenever files change
configure :development do
  activate :livereload
end

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

helpers do
  def fh_url
    build? ? 'https://fh.hypernovastudio.com' : 'https://form-handler.test'
  end

  def truncate_with_complete_sentences(text, options={})
    min_last_word_length = options.delete(:min_last_word_length) || 3
    truncated_text = truncate(text.gsub(/[\s\n]+/, ' ').strip, options)

    cleaned_text =
    if last_complete_sentence_ends_at =
      truncated_text.rindex(/\w{#{min_last_word_length}}[\.\?\!][\s"$]/)
      truncated_text[0..last_complete_sentence_ends_at+min_last_word_length]
    else
      truncated_text.sub(/(\s|[^\s]+)\.{3}$/, '').strip
    end

    if cleaned_text.scan(/"/).count % 2 > 0
      cleaned_text + '"'
    else
      cleaned_text
    end
  end
end

activate :directory_indexes

activate :blog do |blog|
  blog.name = "rentals"
  # This will add a prefix to all links, template references and source paths
  # blog.prefix = "rentals"

  blog.custom_collections = {
    category: {
      link: '/{category}.html',
      template: '/rentals/category.html'
    }
  }

  blog.permalink = "{category}/{slug}.html"
  # Matcher for blog source files
  blog.sources = "rentals/{category}/{slug}.html"
  # blog.taglink = "tags/{tag}.html"
  blog.layout = "rental"
  # blog.summary_separator = /(READMORE)/
  # blog.summary_length = 250
  # blog.year_link = "{year}.html"
  # blog.month_link = "{year}/{month}.html"
  # blog.day_link = "{year}/{month}/{day}.html"
  # blog.default_extension = ".markdown"

  # blog.tag_template = "tag.html"
  # blog.calendar_template = "calendar.html"

  # Enable pagination
  # blog.paginate = true
  # blog.per_page = 10
  # blog.page_link = "page/{num}"end
end

::Sass.load_paths << 'node_modules/bootstrap-sass/assets/stylesheets'
::Sass.load_paths << 'node_modules/photoswipe/src/css'

activate :external_pipeline,
  name: :parcel,
  command: build? ? 'NODE_ENV=production yarn build-js' : 'yarn watch',
  source: 'tmp/assets',
  latency: 1

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

configure :build do
  # Minify CSS on build
  activate :minify_css

  # Minify HTML on build
  activate :minify_html do |html|
    html.remove_input_attributes  = false
    html.preserve_line_breaks     = true
  end

  # Gzip everything
  activate :gzip

  # Fingerprints on assets
  activate :asset_hash
end

activate :s3_sync
