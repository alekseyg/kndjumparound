require 'base64'
require 'imgkit'
require 'erb'

class Image
  STYLES = {
    '330' => {
      width:  330,
      height: 330
    },
    '1125' => {
      width:  1125,
      height: 1125
    }
  }

  def initialize(image_path, style)
    @image_path = image_path
    style_attrs = STYLES[style]
    @style      = style
    @width      = style_attrs[:width]
    @height     = style_attrs[:height]
    fetch_assets
  end

  def html
    @html ||= render_html
  end

  def jpg
    @jpg ||= render_jpg
  end

  private
    def render_html
      template_path = File.join(File.dirname(__FILE__), 'image.html.erb')
      renderer = ERB.new(File.read(template_path))
      renderer.result(binding)
    end

    def render_jpg
      kit = IMGKit.new(html, quality: 65, width: @width, height: @height)
      kit.to_jpg
    end

    def fetch_assets
      asset_files = {
        bg: @image_path,
        logo: File.join(File.dirname(__FILE__),
          '..', '..', 'source', 'images', 'logo.png')
      }

      @assets = asset_files.map do |key, value|
        [ key, Base64.strict_encode64(File.binread(value)) ]
      end.to_h
    end
end
