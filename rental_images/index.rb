#!/usr/bin/env ruby

DEBUG = ARGV.include?('--debug')
ROOT  = File.dirname(__FILE__)

require 'yaml'
require 'digest'
require 'fileutils'
require File.join(ROOT, 'app', 'image')

digests_file = File.join(ROOT, 'digests.yml')
digests = YAML.load_file(digests_file) rescue {}

Dir.glob(File.join(ROOT, 'rentals', '**', '*.jpg')) do |file|
  digest = Digest::MD5.hexdigest(File.read(file))
  next if digest == digests[file]
  digests[file] = digest

  filename  = File.basename(file)
  outdir    = File.join(ROOT, '..', 'source', 'images', File.dirname(file))

  %w(330 1125).each do |style|
    styledir  = File.join(outdir, style)
    outfile   = File.join(styledir, filename)

    print "Creating file: #{outfile} ..."

    FileUtils.mkdir_p(styledir)

    image = Image.new(file, style)

    if DEBUG
      htmlfile = File.basename(file, '.jpg') + '.html'
      File.write(File.join(styledir, htmlfile), image.html)
    end

    File.binwrite(outfile, image.jpg)

    print "Done!\n"
  end
end

File.write(digests_file, digests.to_yaml)
